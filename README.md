# Greybus Testbench

## Build Instructions

```
git clone https://git.beagleboard.org/gsoc/greybus/greybus-testbench.git
cd greybus-testbench
mkdir build && cd build
cmake ..
```

## Run Instructions

1. Build the zephyr native POSIX application from https://git.beagleboard.org/gsoc/greybus/greybus-for-zephyr/-/tree/native_posix
    ```
    west build -b native_posix -d build/greybus/uart modules/lib/greybus/samples/subsys/greybus/uart -p
    ```
2. Run the POSIX application
   ```
   ./build/greybus/uart/zephyr/zephyr.exe
   ```
3. Run the testbench over the pty created by the zephyr POSIX application
   ```
   ./greybus_testbench -p /dev/pts/X
   ```